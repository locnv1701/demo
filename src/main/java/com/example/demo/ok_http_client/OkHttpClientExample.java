package com.example.demo.ok_http_client;

import com.example.demo.model.Student;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;

public class OkHttpClientExample {
    private static final OkHttpClient client = new OkHttpClient();
    private static final ObjectMapper objectMapper = new ObjectMapper();
    private static final String GET_STUDENT_URL = "http://localhost:8080/api/v1/student";

    public static void main(String[] args) {
        listStudent();
    }

    public static void listStudent() {
        Request request = new Request.Builder().url(GET_STUDENT_URL).get().build();
        try {
            Response response = client.newCall(request).execute();
            String res = Objects.requireNonNull(response.body()).string();
            System.out.println(res);

            ArrayList<Student> studentArrayList = objectMapper.readValue(res, new TypeReference<ArrayList<Student>>() {});

            studentArrayList.forEach(System.out::println);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

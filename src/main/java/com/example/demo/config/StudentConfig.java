package com.example.demo.config;

import com.example.demo.repository.StudentRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class StudentConfig {

    @Bean
    CommandLineRunner commandLineRunner(StudentRepository repository) {
        return args -> {
//            Student quinn = new Student(
//                    1L,
//                    "Quinn",
//                    "quinn@gmail.com",
//                    LocalDate.of(2001, Month.AUGUST, 27)
//            );
//            Student jennie = new Student(
//                    "Jennie",
//                    "jennierubyjane@gmail.com",
//                    LocalDate.of(1996, Month.JANUARY, 16)
//            );

            repository.findAll();
        };
    }
}